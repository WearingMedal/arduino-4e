int red = 11;
int green = 5;
int blue = 6;

void changeColor(int r, int g, int b) {
	analogWrite(red, r);
 	analogWrite(green, g);
	analogWrite(blue, b);
}

void getColor(int *c) {
	while (!Serial.available()) {};
 	*c = Serial.parseInt();
 	Serial.println(*c);
}

void setup() {
	Serial.begin(9600);

	pinMode(red, OUTPUT);
	pinMode(green, OUTPUT);
	pinMode(blue, OUTPUT);

	Serial.println("= Seriale Avviato =");
}

void loop() {
	changeColor(0, 0, 0);
	int r = 0, g = 0, b = 0;

	Serial.print("Inserisci il valore del rosso: ");
	getColor(&r);

	Serial.print("Inserisci il valore del verde: ");
	getColor(&g);

	Serial.print("Inserisci il valore del blu: ");
	getColor(&b);

	changeColor(r, g, b);
	delay(5000);
}
