String choice;
int ledPin = 13;

void setup()
{
    pinMode(ledPin, OUTPUT);

    Serial.begin(9600);
    Serial.println("<= Seriale avviato =>");
}

void onOff(String ch)
{
    ch.toLowerCase();
    if (!ch.compareTo("si"))
    {
        digitalWrite(ledPin, HIGH);
    }
    else
    {
        digitalWrite(ledPin, LOW);
    }
}

void loop()
{
    Serial.print("Accendere il led? (SI/NO): ");
    while (!Serial.available())
    {
    }
    choice = Serial.readString();
    Serial.println(choice);
    onOff(choice);
}