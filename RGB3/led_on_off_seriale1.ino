int led = 0;
int val = 13;

void onOff(int v) {
  
  if (v > 1) {
  	digitalWrite(val, HIGH);
  } else {
    digitalWrite(val, LOW);
  }
}

void setup() {
  
  Serial.begin(9600);
  pinMode(val, OUTPUT);
  
  Serial.println("<Arduino Pronto>");
  
}
 
void loop() {
  
  Serial.println("Accendi il Led (valore > 1)): ");
  Serial.println("Oppure spegni il Led (valore <= 0)): ");
  while (Serial.available()==0){}             
  led = Serial.parseInt();
  Serial.println(led);
  
  onOff(led);
  delay(4000);
}
