void setup() {
  Serial.begin(9600); 
  Serial.write("Digitare una stringa: \n");
}

void loop() {
  // check if data is available
  if (Serial.available() > 0) {
    // Legge una stringa in input
    String inString = Serial.readString();

    // Stampa la stringa ricevuta
    Serial.print("Stringa ricevuta: ");
    Serial.println(inString);
  }
}

